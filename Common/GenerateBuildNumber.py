import sys
import os
ENV_MAJOR_VER = os.environ['ENV_MAJOR_VER']
ENV_MINOR_VER = os.environ['ENV_MINOR_VER']
CI_PIPELINE_ID = os.environ['CI_PIPELINE_ID']
print('V' +ENV_MAJOR_VER  + '.' +ENV_MINOR_VER  + '.' + CI_PIPELINE_ID)
sys.exit(0)
