import sys
import os
import json
import http.client

if len(sys.argv) < 3 :
  print('Invalid args')
  sys.exit()

STAGE_NAME = sys.argv[1]
JOB_NAME = sys.argv[2]

ENV_GITLAB_HOST = os.environ['ENV_GITLAB_HOST']
ENV_GITLAB_PORT = os.environ['ENV_GITLAB_PORT']
ENV_PERSONAL_TOKEN = os.environ['ENV_PERSONAL_TOKEN']
CI_PROJECT_ID = os.environ['CI_PROJECT_ID']
CI_PIPELINE_ID = os.environ['CI_PIPELINE_ID']

conn = http.client.HTTPSConnection(ENV_GITLAB_HOST,ENV_GITLAB_PORT)
headers = {
  'Authorization': 'Bearer ' + ENV_PERSONAL_TOKEN
}

conn = http.client.HTTPSConnection(ENV_GITLAB_HOST,ENV_GITLAB_PORT)
conn.request("GET", "/api/v4/projects/" + CI_PROJECT_ID + "/pipelines/" + CI_PIPELINE_ID + "/jobs", '', headers)
res = conn.getresponse()
data = res.read()
jobs = json.loads(data)

for job in jobs:
    if job["name"] == JOB_NAME and job["stage"] == STAGE_NAME:
        JOB_ID = job["id"]

headers = {
  'Authorization': 'Bearer ' + ENV_PERSONAL_TOKEN,
  'Content-Type': 'application/x-www-form-urlencoded'
}

conn = http.client.HTTPSConnection(ENV_GITLAB_HOST,ENV_GITLAB_PORT)
conn.request("POST", "/api/v4/projects/" + CI_PROJECT_ID + "/jobs/" + str(JOB_ID) + "/play", '', headers)
res = conn.getresponse()
if res.status == 200 :
  print('|||||| DONE ||||||')
else :
  print('|||||| DONE - WITH ERROS ||||||')
