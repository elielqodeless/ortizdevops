import sys
import os
import json
import http.client
import subprocess

if len(sys.argv) < 3 :
  print('Invalid args')
  sys.exit()

#PIPELINE CONSTANTS 
ENV_PORTAL_CI_SERVER_HOST = os.environ['ENV_PORTAL_CI_SERVER_HOST']
ENV_PORTAL_CI_SERVER_PORT = os.environ['ENV_PORTAL_CI_SERVER_PORT']
ENV_DEPLOY_URL = os.environ['ENV_DEPLOY_URL']
ENV_DEPLOY_TYPE = os.environ['ENV_DEPLOY_TYPE']
ENV_CHANNEL_TYPE = os.environ['ENV_CHANNEL_TYPE']
ENV_CHANNEL_ID = os.environ['ENV_CHANNEL_ID']
ENV_SLACK_USER_ID = os.environ['ENV_SLACK_USER_ID']
ENV_SLACK_OWNER_USER = os.environ['ENV_SLACK_OWNER_USER']
ENV_PORTAL_CI_HTTPS = os.environ['ENV_PORTAL_CI_HTTPS']
ENV_PORTAL_CI_HTTPS = os.environ['ENV_PORTAL_CI_HTTPS']
ENV_NEXUS_USERNAME = os.environ['ENV_NEXUS_USERNAME']
ENV_NEXUS_PASSWORD = os.environ['ENV_NEXUS_PASSWORD']
ENV_NEXUS_HOST = os.environ['ENV_NEXUS_HOST']
ENV_NEXUS_PORT = os.environ['ENV_NEXUS_PORT']
ENV_IMAGE_NAME = os.environ['ENV_IMAGE_NAME']
ENV_IMAGE_ID = os.environ['ENV_IMAGE_ID']
ENV_GITLAB_HOST = os.environ['ENV_GITLAB_HOST']
ENV_GITLAB_PORT = os.environ['ENV_GITLAB_PORT']
ENV_PERSONAL_TOKEN = os.environ['ENV_PERSONAL_TOKEN']
CI_PROJECT_ID = os.environ['CI_PROJECT_ID']
CI_PIPELINE_ID = os.environ['CI_PIPELINE_ID']
CI_JOB_ID = os.environ['CI_JOB_ID']
CI_COMMIT_SHORT_SHA = os.environ['CI_COMMIT_SHORT_SHA']
CI_COMMIT_REF_NAME = os.environ['CI_COMMIT_REF_NAME']
CI_PROJECT_NAME = os.environ['CI_PROJECT_NAME']
GITLAB_USER_EMAIL =  os.environ['GITLAB_USER_EMAIL']


#PIPELINE CONSTANTS
PARAM_BUILD_NUMBER = sys.argv[1]
# PARAM_TARGET_BRANCH = sys.argv[2]
QA_JOB_ID = sys.argv[2]

CONST_HEADERS = {
  'Authorization': 'Bearer ' + ENV_PERSONAL_TOKEN
}

# print('==> TARGET BRANCH ' + PARAM_TARGET_BRANCH)
print('==> BUILD NUMBER ' + PARAM_BUILD_NUMBER)
print('==> PROJECT ID ' + CI_PROJECT_ID)

#GET ACTIVE JOBS
conn = http.client.HTTPSConnection(ENV_GITLAB_HOST,ENV_GITLAB_PORT)
conn.request("GET", "/api/v4/projects/" + CI_PROJECT_ID +"/merge_requests?state=merged", '', CONST_HEADERS)
# conn.request("GET", "/api/v4/projects/" + CI_PROJECT_ID +"/merge_requests?state=merged&target_branch=" + PARAM_TARGET_BRANCH, '', CONST_HEADERS)
res = conn.getresponse()
data = res.read()

mrs = []
mrList = []
if res.status == 200 :

  items = json.loads(data)
  print(len(items))
  
  # PLAY A DEPLOY
  for item in items:
      bMergeOK = False
      if(item["state"] == 'merged' and len(item["labels"]) == 0):
        # CREATE LIST OF MRS  
        mrs.append(
          {
              "id": item["id"],
              "title": item["title"],
              "description": item["description"]
          })
        bMergeOK = True
      else:
        for label in item["labels"] :
            if (label == 'QA_HOLD') :
              bMergeOK = False
            else :
              bMergeOK = True
              
      if (bMergeOK):
      
          mrList.extend(item)
          print('==> ID '     + str(item["id"]))
          print('==> IID '    + str(item["iid"]))

          # ADD LABEL TO MR
          payload = 'id='+ str(item["id"]) +'&add_labels=QA_READY&merge_request_iid=' + str(item["iid"])
          headers = {
            'Authorization': 'Bearer ' + ENV_PERSONAL_TOKEN,
            'Content-Type': 'application/x-www-form-urlencoded'
          }
          conn = http.client.HTTPSConnection(ENV_GITLAB_HOST,ENV_GITLAB_PORT)
          conn.request("PUT", "/api/v4/projects/" + CI_PROJECT_ID + "/merge_requests/" + str(item["iid"]), payload, headers)
          res = conn.getresponse()

          print('==> API PUT STATUS ' + str(res.status))

          # ADD TAG TO TARGET BRANCH
          if res.status == 200 :
            
            payload = 'id='+ str(item["id"]) +'&add_labels=QA_READY&merge_request_iid=' + str(item["iid"])
            payload = 'id='+ CI_PROJECT_ID +'&tag_name=' + PARAM_BUILD_NUMBER + '&ref=' + str(item["target_branch"])
            headers = {
              'Authorization': 'Bearer ' + ENV_PERSONAL_TOKEN,
              'Content-Type': 'application/x-www-form-urlencoded'
            }
            conn = http.client.HTTPSConnection(ENV_GITLAB_HOST,ENV_GITLAB_PORT)
            conn.request("POST", "/api/v4/projects/" + CI_PROJECT_ID + "/repository/tags", payload, headers)
            res = conn.getresponse()
            if res.status == 201 :
              print('|||||| DONE ||||||')
            else :
              print('|||||| DONE - WITH ERROS ||||||')
          else :
            print("ERROR PLACING THE LABEL")
      else :
        print('==> IGNORED ITEM...')
        print(str(item))
else :
  print('|||||| FATAL ERROR ||||||')

print('|||||| RUNNING STAGE PUBLISH IMAGE ||||||')

#PULL LATEST GAME IMAGE
DockerLogin = "docker login "+ ENV_NEXUS_HOST + ":"+ ENV_NEXUS_PORT +" --username="+ ENV_NEXUS_USERNAME + " --password="+ENV_NEXUS_PASSWORD
print(DockerLogin)
DockerPull = "docker pull " + str(ENV_IMAGE_NAME)
print(DockerPull)

with open("/tmp/output.log", "a") as output:
     print("passo 1")
     print(str(subprocess.call("docker login"+str(ENV_NEXUS_HOST)+":"+str(ENV_NEXUS_PORT)+" --username="+str(ENV_NEXUS_USERNAME)+" --password="+str(ENV_NEXUS_PASSWORD), shell=True, stdout=output, stderr=output)))
     print(str(subprocess.call("docker pull" + str(ENV_IMAGE_NAME), shell=True, stdout=output, stderr=output)))


#MIGRATE ORTIZONE PYTHONS HERE
#MIGRATE ORTIZONE PYTHONS HERE
#MIGRATE ORTIZONE PYTHONS HERE


#PUBLISH NEW GAME IMAGE
DockerTag = "docker tag " + str(ENV_IMAGE_ID) + " " + str(ENV_NEXUS_HOST) + ":" + str(ENV_NEXUS_PORT) + "/" + str(ENV_IMAGE_NAME) + ":" + str(PARAM_BUILD_NUMBER )
print(DockerTag)
DockerPush = "docker push " + str(ENV_NEXUS_HOST) + ":" + str(ENV_NEXUS_PORT) + "/" + str(ENV_IMAGE_NAME) + ":" + str(PARAM_BUILD_NUMBER)
print(DockerPush)
with open("/tmp/output.log", "a") as output:
     print("passo 2")
     print(str(subprocess.call("docker pull" + str(ENV_IMAGE_NAME), shell=True, stdout=output, stderr=output)))
     print(str(subprocess.call("docker tag " + str(ENV_IMAGE_ID) + " " + str(ENV_NEXUS_HOST) + ":" + str(ENV_NEXUS_PORT) + "/" + str(ENV_IMAGE_NAME) + ":" + str(PARAM_BUILD_NUMBER), shell=True, stdout=output, stderr=output)))
     print(str(subprocess.call("docker push " + str(ENV_NEXUS_HOST) + ":" + str(ENV_NEXUS_PORT) + "/" + str(ENV_IMAGE_NAME) + ":" + str(PARAM_BUILD_NUMBER), shell=True, stdout=output, stderr=output)))

print('|||||| RUNNING STAGE BUILD - DEPLOY DEV ||||||')

print('CI_JOB_ID' + CI_JOB_ID)
print('QA_JOB_ID' + QA_JOB_ID)

if ENV_PORTAL_CI_HTTPS.upper() == "YES":
  isGitHttps = True
else:
  isGitHttps = False  

# print(mrList)


#   mrList = [id:item.id, title: item.title, description: item.description]
#   mrs.extend(mrList) 

print(mrs)

#GET ACTIVE JOBS
payload = json.dumps({
  "slackQAUser": str(ENV_SLACK_USER_ID),
  "slackOwnerUser": str(ENV_SLACK_OWNER_USER),
  "gitAuthor": str(GITLAB_USER_EMAIL),
  "gitCommit": str(CI_COMMIT_SHORT_SHA),
  "deployUrl": str(ENV_DEPLOY_URL),
  "deployType": str(ENV_DEPLOY_TYPE),
  "projectId": str(CI_PROJECT_ID),
  "projectName": str(CI_PROJECT_NAME),
  "pipelineId": str(CI_PIPELINE_ID),
  "channelType": str(ENV_CHANNEL_TYPE),
  "channelId": str(ENV_CHANNEL_ID),
  "jobId": str(QA_JOB_ID),
  "gitBranch": str(CI_COMMIT_REF_NAME),
  "buildNumber": str(PARAM_BUILD_NUMBER),
  "gitToken": str(ENV_PERSONAL_TOKEN),
  "gitHost": str(ENV_GITLAB_HOST),
  "isGitHttps": isGitHttps,
    "mRs": mrs
})

print(payload)

headers = {
  'Content-Type': 'application/json',
  'Authorization': 'true'
}

if isGitHttps :
  print("HTTPS")
  conn = http.client.HTTPSConnection(ENV_PORTAL_CI_SERVER_HOST,ENV_PORTAL_CI_SERVER_PORT)
else :
  print("HTTP")
  conn = http.client.HTTPConnection(ENV_PORTAL_CI_SERVER_HOST,ENV_PORTAL_CI_SERVER_PORT)
  
conn.request("POST", "/api/Slack", payload, headers)
res = conn.getresponse()
data = res.read()
print(data.decode("utf-8"))

if res.status == 200 :
  print('|||||| DONE ||||||')
else :
  print('|||||| FATAL ERROR ||||||')
  

