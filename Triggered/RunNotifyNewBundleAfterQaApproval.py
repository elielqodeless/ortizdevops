# -*- coding: UTF-8 -*-

import sys
import os
import json
import http.client

if len(sys.argv) < 1 :
  print('Invalid args')
  sys.exit()

#Variables 
CI_PROJECT_ID = os.environ['CI_PROJECT_ID']
CI_PIPELINE_ID = os.environ['CI_PIPELINE_ID']
CI_JOB_ID = os.environ['CI_JOB_ID']
CI_PROJECT_NAME = os.environ['CI_PROJECT_NAME']
GITLAB_USER_EMAIL =  os.environ['GITLAB_USER_EMAIL']
ENV_DEPLOY_URL = os.environ['ENV_DEPLOY_URL']
ENV_CHANNEL_TYPE = os.environ['ENV_CHANNEL_TYPE']
ENV_CHANNEL_ID = os.environ['ENV_CHANNEL_ID']
ENV_SLACK_USER_ID = os.environ['ENV_SLACK_USER_ID']
ENV_UAT_USER = os.environ['ENV_UAT_USER']

BUILDNUMBER = sys.argv[1]

#NOTIFY QA TEAM BY SLACK 
conn = http.client.HTTPSConnection("slack.com")
payload = 'text=Ol%C3%A1 <@'+ENV_UAT_USER+'>\nA vers%C3%A3o abaixo j%C3%A1 foi aprovada pelo time de QA\n\nJogo: '+CI_PROJECT_NAME+'\nVers%C3%A3o: '+BUILDNUMBER+'&channel='+ENV_CHANNEL_ID

headers = {
  'Content-Type': 'application/x-www-form-urlencoded',
  'Authorization': 'Bearer xoxb-2057794773027-2076539941845-si2kiGtvB9hlF0YTLsG0UBHl'
}
conn.request("POST", "/api/chat.postMessage", payload, headers)
res = conn.getresponse()
data = res.read()
print(data.decode(encoding='UTF-8'))

