# Add Nexus Steps (docker login, docker pull, docker tag, docker push)
import subprocess
import sys
import os

if len(sys.argv) < 2 :
  print('Invalid args')
  sys.exit()

#PIPELINE CONSTANTS	
ENV_GITLAB_HOST = os.environ['ENV_GITLAB_HOST']
ENV_GITLAB_PORT = os.environ['ENV_GITLAB_PORT']
ENV_PERSONAL_TOKEN = os.environ['ENV_PERSONAL_TOKEN']
ENV_NEXUS_USERNAME = os.environ['ENV_NEXUS_USERNAME']
ENV_NEXUS_PASSWORD = os.environ['ENV_NEXUS_PASSWORD']
ENV_NEXUS_HOST = os.environ['ENV_NEXUS_HOST']
ENV_NEXUS_PORT = os.environ['ENV_NEXUS_PORT']
ENV_IMAGE_NAME = os.environ['ENV_IMAGE_NAME']
ENV_IMAGE_ID = os.environ['ENV_IMAGE_ID']
CI_PROJECT_ID = os.environ['CI_PROJECT_ID']
CI_PIPELINE_ID = os.environ['CI_PIPELINE_ID']
CI_JOB_ID = os.environ['CI_JOB_ID']

#PIPELINE CONSTANTS
PARAM_BUILD_NUMBER = sys.argv[1]

#PULL LATEST GAME IMAGE
DockerLogin = "docker login "+ ENV_NEXUS_HOST + ":"+ ENV_NEXUS_PORT +" --username="+ ENV_NEXUS_USERNAME + " --password="+ENV_NEXUS_PASSWORD
print(DockerLogin)
DockerPull = "docker pull " + str(ENV_IMAGE_NAME)
print(DockerPull)

with open("/tmp/output.log", "a") as output:
     print("passo 1")
     print(str(subprocess.call("docker login"+str(ENV_NEXUS_HOST)+":"+str(ENV_NEXUS_PORT)+" --username="+str(ENV_NEXUS_USERNAME)+" --password="+str(ENV_NEXUS_PASSWORD), shell=True, stdout=output, stderr=output)))
     print(str(subprocess.call("docker pull" + str(ENV_IMAGE_NAME), shell=True, stdout=output, stderr=output)))


#MIGRATE ORTIZONE PYTHONS HERE
#MIGRATE ORTIZONE PYTHONS HERE
#MIGRATE ORTIZONE PYTHONS HERE


#PUBLISH NEW GAME IMAGE
DockerTag = "docker tag " + str(ENV_IMAGE_ID) + " " + str(ENV_NEXUS_HOST) + ":" + str(ENV_NEXUS_PORT) + "/" + str(ENV_IMAGE_NAME) + ":" + str(PARAM_BUILD_NUMBER )
print(DockerTag)
DockerPush = "docker push " + str(ENV_NEXUS_HOST) + ":" + str(ENV_NEXUS_PORT) + "/" + str(ENV_IMAGE_NAME) + ":" + str(PARAM_BUILD_NUMBER)
print(DockerPush)
with open("/tmp/output.log", "a") as output:
     print("passo 2")
     print(str(subprocess.call("docker pull" + str(ENV_IMAGE_NAME), shell=True, stdout=output, stderr=output)))
     print(str(subprocess.call("docker tag " + str(ENV_IMAGE_ID) + " " + str(ENV_NEXUS_HOST) + ":" + str(ENV_NEXUS_PORT) + "/" + str(ENV_IMAGE_NAME) + ":" + str(PARAM_BUILD_NUMBER), shell=True, stdout=output, stderr=output)))
     print(str(subprocess.call("docker push " + str(ENV_NEXUS_HOST) + ":" + str(ENV_NEXUS_PORT) + "/" + str(ENV_IMAGE_NAME) + ":" + str(PARAM_BUILD_NUMBER), shell=True, stdout=output, stderr=output)))
