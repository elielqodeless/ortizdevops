import sys
import os
import json
import http.client

if len(sys.argv) < 3 :
  print('Invalid args')
  sys.exit()

print('|||||| RUNNING STAGE BUILD - DEPLOY DEV ||||||')

CI_PROJECT_ID = os.environ['CI_PROJECT_ID']
CI_PIPELINE_ID = os.environ['CI_PIPELINE_ID']
CI_JOB_ID = os.environ['CI_JOB_ID']
CI_COMMIT_SHORT_SHA = os.environ['CI_COMMIT_SHORT_SHA']
CI_COMMIT_REF_NAME = os.environ['CI_COMMIT_REF_NAME']
CI_PROJECT_NAME = os.environ['CI_PROJECT_NAME']
GITLAB_USER_EMAIL =  os.environ['GITLAB_USER_EMAIL']
ENV_PORTAL_CI_SERVER_HOST = os.environ['ENV_PORTAL_CI_SERVER_HOST']
ENV_PORTAL_CI_SERVER_PORT = os.environ['ENV_PORTAL_CI_SERVER_PORT']
ENV_DEPLOY_URL = os.environ['ENV_DEPLOY_URL']
ENV_DEPLOY_TYPE = os.environ['ENV_DEPLOY_TYPE']
ENV_CHANNEL_TYPE = os.environ['ENV_CHANNEL_TYPE']
ENV_CHANNEL_ID = os.environ['ENV_CHANNEL_ID']
ENV_SLACK_USER_ID = os.environ['ENV_SLACK_USER_ID']
ENV_PORTAL_CI_HTTPS = os.environ['ENV_PORTAL_CI_HTTPS']
ENV_PERSONAL_TOKEN = os.environ['ENV_PERSONAL_TOKEN']
ENV_GITLAB_HOST = os.environ['ENV_GITLAB_HOST']
ENV_PORTAL_CI_HTTPS = os.environ['ENV_PORTAL_CI_HTTPS']




#PIPELINE CONSTANTS
BUILDNUMBER = sys.argv[1]
QA_JOB_ID = sys.argv[2]

print('CI_JOB_ID' + CI_JOB_ID)
print('QA_JOB_ID' + QA_JOB_ID)

if ENV_PORTAL_CI_HTTPS.upper() == "YES":
  isGitHttps = True
else:
  isGitHttps = False  

#GET ACTIVE JOBS
payload = json.dumps({
  "slackUser": str(ENV_SLACK_USER_ID),
  "gitAuthor": str(GITLAB_USER_EMAIL),
  "gitCommit": str(CI_COMMIT_SHORT_SHA),
  "deployUrl": str(ENV_DEPLOY_URL),
  "deployType": str(ENV_DEPLOY_TYPE),
  "projectId": str(CI_PROJECT_ID),
  "projectName": str(CI_PROJECT_NAME),
  "pipelineId": str(CI_PIPELINE_ID),
  "channelType": str(ENV_CHANNEL_TYPE),
  "channelId": str(ENV_CHANNEL_ID),
  "jobId": str(QA_JOB_ID),
  "gitBranch": str(CI_COMMIT_REF_NAME),
  "buildNumber": str(BUILDNUMBER),
  "gitToken": str(ENV_PERSONAL_TOKEN),
  "gitHost": str(ENV_GITLAB_HOST),
  "isGitHttps": isGitHttps
})
headers = {
  'Content-Type': 'application/json',
  'Authorization': 'true'
}

if ENV_PORTAL_CI_HTTPS == "YES" :
  conn = http.client.HTTPSConnection(ENV_PORTAL_CI_SERVER_HOST,ENV_PORTAL_CI_SERVER_PORT)
else :
  conn = http.client.HTTPConnection(ENV_PORTAL_CI_SERVER_HOST,ENV_PORTAL_CI_SERVER_PORT)
  
conn.request("POST", "/api/Slack", payload, headers)
res = conn.getresponse()
data = res.read()
print(data.decode("utf-8"))

if res.status == 200 :
  print('|||||| DONE ||||||')
else :
  print('|||||| FATAL ERROR ||||||')
